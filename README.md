## docker-symfony

for developing applications in a local environment
 
## Included

- symfony 5.3.*
- php7.4-fpm
- nginx:stable-alpine
- mysql:5.7
- adminer:4.8.1


### configure .env.local
    DATABASE_URL="mysql://symfony:symfony@database:3306/symfony_docker?serverVersion=5.7"